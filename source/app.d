import std.stdio;
import std.conv;
import derelict.sdl2.sdl;
import derelict.sdl2.image;

struct Camera {
    int x;
    int y;
}

class MapViewer {
    SDL_Window *win;
    SDL_Renderer *ren;
    SDL_Surface *sur;
    SDL_Texture *tex;
    float zoom = 1.;
    Camera camera;

    bool init () {
        DerelictSDL2.load();
        DerelictSDL2Image.load();

        if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
            writeln("SDL_Init Error: ", to!string(SDL_GetError()));
            return false;
        }
        
        if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) {
            writeln("IMG_Init Error: ", to!string(SDL_GetError()));
            return false;
        }

        win = SDL_CreateWindow("mapviewer", 100, 100, 600, 600, SDL_WINDOW_SHOWN);
        if (win == null) {
            writeln("SDL_CreateWindow Error: ", to!string(SDL_GetError()));
            return false;
        }

        ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
        if (ren == null) {
            writeln("SDL_CreateRenderer Error: ", to!string(SDL_GetError()));
            return false;
        }
    
        sur = IMG_Load("../mapmerger/tiles/2014-03-14 00.11.59/tile_0_0.png");
        if (sur == null) {
            writeln("IMG_Load Error: ", to!string(SDL_GetError()));
            return false;
        }
        
        tex = SDL_CreateTextureFromSurface(ren, sur);
        SDL_FreeSurface(sur);
        if (tex == null) {
            writeln("SDL_CreateTextureFromSurface Error: ", to!string(SDL_GetError()));
            return false;
        }
        
        /*
        SDL_Rect topLeftViewport;
        topLeftViewport.x = 0;
        topLeftViewport.y = 0;
        topLeftViewport.w = 120;
        topLeftViewport.h = 120;
        SDL_RenderSetViewport(ren, &topLeftViewport);
        */

        return true;
    }
    
    void run () {
        //SDL_Delay(10000);
        SDL_Event e;
        bool quit = false;
        while (!quit) {
            while (SDL_PollEvent(&e)) {
                if (e.type == SDL_QUIT)
                    quit = true;
                if (e.type == SDL_MOUSEMOTION) {
                    if (e.motion.state & SDL_BUTTON_RMASK) {
                        if (e.motion.yrel >= 0)
                            zoom *= 1. + cast(float)(e.motion.yrel)/100.;
                        else
                            zoom /= 1. + cast(float)(-e.motion.yrel)/100.;
                    } else if (e.motion.state & SDL_BUTTON_LMASK) {
                        camera.x += e.motion.xrel;
                        camera.y += e.motion.yrel;
                    }
                }
            }
            //Render the scene
            SDL_RenderClear(ren);

            const TEXSZ = 100.;
            const SCRSZ = 200.;
            auto size = TEXSZ * zoom;
            auto x = (SCRSZ - size)/2;
            
            SDL_Rect drect;
            drect.x = (cast(uint)x)+camera.x;
            drect.y = (cast(uint)x)+camera.y;
            drect.w = cast(uint)(size);
            drect.h = cast(uint)(size);
            SDL_RenderCopy(ren, tex, null, &drect);
            
            /*
            SDL_SetRenderDrawColor(ren, 0xFF, 0xFF, 0x00, 0xFF);
            for (uint i=0; i<200; i+=2) {
                SDL_RenderDrawPoint(ren, 100, i);
            }
            SDL_SetRenderDrawColor(ren, 0x00, 0x00, 0x00, 0x00);
            */
            
            SDL_RenderPresent(ren);
        }
    }
    
    ~this () {
        if (tex) SDL_DestroyTexture(tex);
        if (ren) SDL_DestroyRenderer(ren);
        if (win) SDL_DestroyWindow(win);
        IMG_Quit();
        SDL_Quit();
    }
};

int main () {
    auto mv = new MapViewer();
    if (mv.init()) mv.run();
    return 0;
}
